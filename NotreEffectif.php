<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="NotreEffectif.css">
    <script src="Bouton.js"></script>
    <title>Document</title>
</head>
<body>
    <?php include("header.php"); ?>
    <!-- Hero header -->
    <section class="hero_header">
        <h1>Notre effectif</h1>
    </section>

 <section class="terrainAndEffectif">
    <section class="terrain">
            <div class="joueur">
                <img src="img/GK.png" alt="">
                <img src="img/GK.png" alt="">
                <img src="img/GK.png" alt="">
            </div>
                <div class="btn">
                    <button id="btnFW"></button>
                </div>
            <div class="joueur">
                <img src="img/GK.png" alt="">
                <img src="img/GK.png" alt="">
                <img src="img/GK.png" alt="">
            </div>
                <div class="btn">
                    <button id="btnMID"></button>
                </div>
            <div class="joueur">
                <img src="img/GK.png" alt="">
                <img src="img/GK.png" alt="">
                <img src="img/GK.png" alt="">
                <img src="img/GK.png" alt="">
            </div>
                <div class="btn">
                    <button id="btnDEF"></button>
                </div>
            <div class="joueur">
            <img src="img/GK.png" alt="">
            </div>
                <div class="btn">
                <button id="btnGK" onclick="replaceclick()"></button>
                </div>
    </section>

    <section id="removeclick" class="effectif">
        <div class="stcol">
            <div class="indiv">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Gardien</span></p>
                </div>
            </div>
            <div class="indiv">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Defenseur</span></p>
                </div>
            </div>
            <div class="indiv">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Defenseur</span></p>
                </div>
            </div>
            <div class="indiv">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Defenseur</span></p>
                </div>
            </div>
        </div>



        <div class="ndcol">
            <div class="indiv">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Defenseur</span></p>
                </div>
            </div>
            <div class="indiv">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Milieu</span></p>
                </div>
            </div>
            <div class="indiv">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Milieu</span></p>
                </div>
            </div>
            <div class="indiv">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Milieu</span></p>
                </div>
            </div>
        </div>



            <div class="rdcol">
                <div class="indiv">
                    <img src="img/GK.png" alt="">
                    <div>
                        <p>1 | Nom Prénom</p>
                        <p><span>Attaquant</span></p>
                    </div>
                </div>
                <div class="indiv">
                    <img src="img/GK.png" alt="">
                    <div>
                        <p>1 | Nom Prénom</p>
                        <p><span>Attaquant</span></p>
                    </div>
                </div>
                <div class="indiv">
                    <img src="img/GK.png" alt="">
                    <div>
                        <p>1 | Nom Prénom</p>
                        <p><span>Attaquant</span></p>
                    </div>
                </div>
                </div>

            <!-- OnClick -->

        <div class="GK">
            <div class="indiv2">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Gardien</span></p>
                </div>
            </div>
        </div>

        <div class="DEF">
            <div class="indiv2">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Defenseur</span></p>
                </div>
            </div>
            <div class="indiv2">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Defenseur</span></p>
                </div>
            </div>
            <div class="indiv2">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Defenseur</span></p>
                </div>
            </div>
            <div class="indiv2">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Defenseur</span></p>
                </div>
            </div>
        </div>    

        <div class="MID">
            <div class="indiv2">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Milieu</span></p>
                </div>
            </div>
            <div class="indiv2">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Milieu</span></p>
                </div>
            </div>
            <div class="indiv2">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Milieu</span></p>
                </div>
            </div>
        </div>

        <div class="FW">
            <div class="indiv2">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Attaquant</span></p>
                </div>
            </div>
            <div class="indiv2">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Attaquant</span></p>
                </div>
            </div>
            <div class="indiv2">
                <img src="img/GK.png" alt="">
                <div>
                    <p>1 | Nom Prénom</p>
                    <p><span>Attaquant</span></p>
                </div>
            </div>
        </div>


    </section>
</section>   
</body>
</html>