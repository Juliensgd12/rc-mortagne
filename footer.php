<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <link rel="stylesheet" href="footer.css">
    <link rel="stylesheet" href="styles.css">
    <title>Document</title>
</head>
<body>
    <footer>
        <div class="contact">
            <div class="coordonnées"> 
            <img src="img/tel.png" alt="">
            <p>03 00 00 00 00</p>
            </div>
            <div class="coordonnées"> 
            <img src="img/mail.png" alt="">
            <p>RCMortagne@gmail.com</p>
            </div>
            <div class="RS"> 
            <p>Suivez-nous</p>
            <div class="logoRS">
            <img src="img/fb.png" alt="">
            <img src="img/twitter.png" alt="">
            <img src="img/insta.png" alt="">
            </div>
            </div>
        </div>


        <div class="logoRC">
            <img src="img/logo.png" alt="">
        </div>

        <div class="footerlink">
            <ul>
                <li><a href="NotreEffectif.php">Notre effectif</a></li>
                <li><a href="billeterie.php">Billeterie</a></li>
                <li><a href="formulaire.php">Nous contacter</a></li>
                <li><a href="#">Mentions légales</a></li>
            </ul>
        </div>

    </footer>
</body>
</html>