<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="Accueil.css">
    <link rel="stylesheet" href="styles.css">
    <title>Accueil</title>
</head>
<?php include("header.php"); ?>
<body>
    <!-- Hero header -->
    <section class="hero_header">
        <h1>Racing Club Mortagne</h1>
    </section>

<!-- 1st section -->

    <section class="lastnews">
        <h2>Nos dernières actualitées</h2>
        <div class="allcards">
            <div class="card">
                <img src="img/card1.png" alt="">
                <div class="txtcard">
                    <h4>Racing Club Mortagne</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                         incididunt ut labore et dolore magna aliqua. </p>
                    <a href="#">En savoir plus</a>
                </div>
            </div>
            <div class="card">
                <img src="img/card2.png" alt="">
                <div class="txtcard">
                    <h4>Racing Club Mortagne</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                         incididunt ut labore et dolore magna aliqua. </p>
                    <a href="#">En savoir plus</a>
                </div>
            </div>
            <div class="card">
                <img src="img/card3.png" alt="">
                <div class="txtcard">
                    <h4>Racing Club Mortagne</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                         incididunt ut labore et dolore magna aliqua. </p>
                    <a href="#">En savoir plus</a>
                </div>
            </div>
        </div>
    </section>
    <!-- 2nd section  -->
    <section class="discover">
        <h2>Découvrez Mortagne</h2>
        <div class="discover2">
            <img src="img/pres.png" alt="">
            <div class="rightPC">
            <h4>Le mot du president</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
            </p>
            </div>
        </div>
    </section>

    <!-- 3rd section -->

    <section class="ticket">
        <h2>Billeterie</h2>
        <div class="leftcardPC">
            <div class="cardticketred">
                <p class="child">Enfant 0 à 2ans</p>
                <p class="tarif">Gratuit</p>
                <a href="billeterie.php">Réserver</a>
            </div>
            <div class="cardticketyellow">
                <p class="child">Enfant 2 à 8ans</p>
                <p class="tarif">12,50 €</p>
                <a href="billeterie.php">Réserver</a>
            </div>
        </div>    
        <div class="rightcardPC">
            <div class="cardticketred">
                <p class="child">Plus de 8 ans</p>
                <p class="tarif">13,50 €</p>
                <a href="billeterie.php">Réserver</a>
            </div>
            <div class="cardticketyellow">
                <p class="child">Adultes (Plus de 18ans)</p>
                <p class="tarif">15 €</p>
                <a href="billeterie.php">Réserver</a>
            </div>
        </div>
        <a class="btnticket" href="billeterie.php">Billeterie</a>
    </section>

    <!-- 4th section -->
    <section class="sponsors">
        <h2>Nos sponsors</h2>
        <div class="logosponsors">
            <img src="img/creditNord.png" alt="">
            <img src="img/auchan.png" alt="">
            <img src="img/cf_mkt.png" alt="">
        </div>

    </section>
</body>
<?php include("footer.php"); ?>
</html>