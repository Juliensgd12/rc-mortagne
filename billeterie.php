<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="billeterie.css">
    <title>Billeterie</title>
</head>
<body>
    <?php include("header.php"); ?>
    <!-- Hero header -->
    <section class="hero_header">
        <h1>Billeterie</h1>
    </section>
    
    <!-- Card Tarif -->

    <section class="ticket">
        <h2>Billeterie</h2>
        <div class="allcardsPC">
            <div class="leftcardPC">
                <div class="cardticketred">
                    <p class="child">Enfant 0 à 2ans</p>
                    <p class="tarif">Gratuit</p>
                </div>
                <div class="cardticketyellow">
                    <p class="child">Enfant 2 à 8ans</p>
                    <p class="tarif">12,50 €</p>
                </div>
            </div>    
            <div class="rightcardPC">
                <div class="cardticketred">
                    <p class="child">Plus de 8 ans</p>
                    <p class="tarif">13,50 €</p>
                </div>
                <div class="cardticketyellow">
                    <p class="child">Adultes (Plus de 18ans)</p>
                    <p class="tarif">15 €</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Choixtarif -->
    <section class="pricechoice">
        <h2>Choisissez votre tarif</h2>

        <!-- 1er tarif -->
        <div class="cardtarif">
            <div class="bgred">
            <p>Enfants 0 à 2 ans</p>
            </div>
            <div class="txtcardtarif">
            <p class="prix1">Gratuit</p>
            </div>
            <div class="btnqte">
                    <button type="button" class=" qty-moins1"><img src="img/-.png" alt=""></button>
                    <input type="text" class="qty1" value="0">
                    <button type="button" class="qty-plus1"><img src="img/+.png" alt=""></button>
                </div>
                <div class="txtcardtarif2">
            <p class="sommesArticles1">0 €</p>
            </div>
        </div>

        <!-- 2ème tarif -->
        <div class="cardtarifyellow">
            <div class="bgyellow">
            <p>Enfants 2 à 8 ans</p>
            </div>
            <div class="txtcardtarif">
            <p class="prix2">12.50 €</p>
            </div>
            <div class="btnqte">
                <button type="button" class="less2"><img src="img/-.png" alt=""></button>
                <input type="text" class="qty2" value="0">
                <button type="button" class="more2"><img src="img/+.png" alt=""></button>
            </div>
            <div class="txtcardtarif2">
                <p class="sommesArticles2">0 €</p>
                </div>
        </div>

        <!-- 3ème tarif -->
        <div class="cardtarif">
            <div class="bgred">
            <p>Plus de 8 ans</p>
            </div>
            <div class="txtcardtarif">
            <p class="prix3">13,50€</p>
            </div>
            <div class="btnqte">
                <button type="button" class="less3"><img src="img/-.png" alt=""></button>
                <input type="text" class="qty3" value="0">
                <button type="button" class="more3"><img src="img/+.png" alt=""></button>
            </div>
            <div class="txtcardtarif2">
                <p class="sommesArticles3">0 €</p>
                </div>
        </div>

        <!-- 4ème tarif -->
        <div class="cardtarifyellow">
            <div class="bgyellow">
            <p>Adultes <i>(Plus de 18ans)</i></p>
            </div>
            <div class="txtcardtarif">
            <p class="prix4">15€</p>
            </div>
            <div class="btnqte">
                <button type="button" class="less4"><img src="img/-.png" alt=""></button>
                <input type="text" class="qty4" value="0">
                <button type="button" class="more4"><img src="img/+.png" alt=""></button>
            </div>
            <div class="txtcardtarif2">
                <p class="sommesArticles4">0 €</p>
                </div>
        </div>

        <!-- Total -->
        <div class="line"></div>

        <div class="cardtarif">
            <div class="total">
            <p>Total</p>
            </div>
                <div class="txtcardtarif">
                <input type="text" name="total" class="totalInput">
            </div>
            <p class="total2">Total = 0 €</p>
        </div>
    </section>

    <script src="Billeterie.js"></script>
</body>
<?php include("footer.php"); ?>
</html>