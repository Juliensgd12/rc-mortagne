<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="Nouslocaliser.css">
    <title>Document</title>
</head>
<body>
    <?php include("header.php"); ?>
    <!-- Hero header -->
    <section class="hero_header">
        <h1>Nous localiser</h1>
    </section>
    
    <section class="stsection">
    <div class="allinfos">
        <div class="infos">
            <img src="img/ping.png" alt="">
            <p>Rue des Anciens d'Afrique du N<br>
                59230 Saint-Amand-les-Eaux
            </p>
        </div>
        <div class="infos">
            <img src="img/telyellow.png" alt="">
            <p> 03 00 00 00 00</p>
        </div>
        <div class="infos">
            <img src="img/mailyellow.png" alt="">
            <p>RCMortagne@gmail.com</p>
        </div>
    </div>
        <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8989.22395943131!2d3.432104563699623!3d50.444919077305805!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2e89381425177%3A0x69210c634234536a!2sRue%20des%20Anciens%20d&#39;Afrique%20du%20N%2C%2059230%20Saint-Amand-les-Eaux!5e0!3m2!1sfr!2sfr!4v1670956132326!5m2!1sfr!2sfr" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
    
    </section>
</body>
<?php include("footer.php"); ?>
</html>