<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="header.css">
    <title>Document</title>
</head>
<body>
    <header>
        <section class="header">
            <img class="logo-header" src="img/logo.png" alt="Logo Mortagne">
            <div class="nav-bar">
                <ul>
                    <li><a href="Accueil.php">Accueil</a></li>
                    <li><a href="NotreEffectif.php">Nos équipes</a></li>
                    <li><a href="#">Evenements</a></li>
                    <li><a href="billeterie.php">Billeterie</a></li>
                    <li><a href="Nous localiser.php">Plan d'accès</a></li>
                    <li><a href="formulaire.php">Nous contacter</a></li>
                </ul>
            </div>
        </section>
    </header>
</body>
</html>