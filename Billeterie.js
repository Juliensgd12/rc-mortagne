//boutons plus et moins


let btnPlus1 = document.querySelector(".qty-plus1");
let btnMoins1 = document.querySelector(".qty-moins1");

// le bouton plus 


btnPlus1.addEventListener('click', increaseQuantity);


function increaseQuantity() {
    document.querySelector('.qty1').value = parseInt(document.querySelector('.qty1').value) + 1;
sommesArticles1();
}

// le bouton moins

btnMoins1.addEventListener('click', decreaseQuantity);

function decreaseQuantity() {
    if (document.querySelector('.qty1').value > 0) {
        document.querySelector('.qty1').value = parseInt(document.querySelector('.qty1').value) - 1;
    }
    sommesArticles1();
}

// l'addition de la sommes des memes artilces 

function sommesArticles1(){
    let prix = parseInt(document.querySelector('.prix1').innerHTML);
    let qty = parseInt(document.querySelector('.qty1').value);
    let sommesAtc = prix*qty;

    document.querySelector('.sommesArticles1').innerHTML = sommesAtc + ' €';

    calcTotal();
}

// BOUTON 2

//boutons plus et moins


let btnPlus2 = document.querySelector(".more2");
let btnMoins2 = document.querySelector(".less2");

// le bouton plus 


btnPlus2.addEventListener('click', increaseQuantity2);


function increaseQuantity2() {
    document.querySelector('.qty2').value = parseInt(document.querySelector('.qty2').value) + 1;
sommesArticles2();
}

// le bouton moins

btnMoins2.addEventListener('click', decreaseQuantity2);

function decreaseQuantity2() {
    if (document.querySelector('.qty2').value > 0) {
        document.querySelector('.qty2').value = parseInt(document.querySelector('.qty2').value) - 1;
    }
    sommesArticles2();
}

// l'addition de la sommes des memes articles 

function sommesArticles2(){
    let prix2 = parseInt(document.querySelector('.prix2').innerHTML);
    let qty2 = parseInt(document.querySelector('.qty2').value);
    let sommesAtc2 = prix2*qty2;

    document.querySelector('.sommesArticles2').innerHTML = sommesAtc2 + ' €';

    calcTotal();
}

// BOUTON 3
//boutons plus et moins


let btnPlus3 = document.querySelector(".more3");
let btnMoins3 = document.querySelector(".less3");

// le bouton plus 


btnPlus3.addEventListener('click', increaseQuantity3);

 function increaseQuantity3() {
   document.querySelector('.qty3').value = parseInt(document.querySelector('.qty3').value) + 1;

   sommesArticles3()
    }

    // le bouton moins

btnMoins3.addEventListener('click', decreaseQuantity3);

function decreaseQuantity3() {
     
    if (document.querySelector('.qty3').value > 0) {
        document.querySelector('.qty3').value = parseInt(document.querySelector('.qty3').value) - 1;
    }
    sommesArticles3()
}

// l'addition de la sommes des memes articles 

function sommesArticles3(){
    let prix3 = parseInt(document.querySelector('.prix3').innerHTML);
    let qty3 = parseInt(document.querySelector('.qty3').value);
    let sommesAtc3 = prix3*qty3;

    document.querySelector('.sommesArticles3').innerHTML = sommesAtc3 + ' €';

    calcTotal();
}


// BOUTON 4
let btnPlus4 = document.querySelector(".more4");
let btnMoins4 = document.querySelector(".less4");

// le bouton plus 


btnPlus4.addEventListener('click', increaseQuantity4);

 function increaseQuantity4() {
   document.querySelector('.qty4').value = parseInt(document.querySelector('.qty4').value) + 1;

   sommesArticles4()
    }

    // le bouton moins

btnMoins4.addEventListener('click', decreaseQuantity4);

function decreaseQuantity4() {
     
    if (document.querySelector('.qty4').value > 0) {
        document.querySelector('.qty4').value = parseInt(document.querySelector('.qty4').value) - 1;
    }
    sommesArticles4()
}
// l'addition de la sommes des memes artilces 

function sommesArticles4(){
    let prix4 = parseInt(document.querySelector('.prix4').innerHTML);
    let qty4 = parseInt(document.querySelector('.qty4').value);
    let sommesAtc4 = prix4*qty4;

    document.querySelector('.sommesArticles4').innerHTML = sommesAtc4 + ' €';

    calcTotal();
}



// Total

function calcTotal() {
    let sommesArti1 = parseInt(document.querySelector('.sommesArticles1').innerHTML);
    let sommesArti2 = parseInt(document.querySelector('.sommesArticles2').innerHTML);
    let sommesArti3 = parseInt(document.querySelector('.sommesArticles3').innerHTML);
    let sommesArti4 = parseInt(document.querySelector('.sommesArticles4').innerHTML);
    let sommesttl = sommesArti1+sommesArti2+sommesArti3+sommesArti4;

    document.querySelector('.total2').innerHTML = 'Total = '+ sommesttl + '€';
    document.querySelector('.totalInput').value = sommesttl + '€';
}
