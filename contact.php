<?php 
    require_once(__DIR__ . '/vendor/autoload.php');
    use \Mailjet\Resources;
    // define('API_PUBLIC_KEY', 'YOUR_KEY');
    // define('API_PRIVATE_KEY', 'YOUR_KEY');
    $mj = new \Mailjet\Client('****************************1234','****************************abcd',true,['version' => 'v3.1']);


    if(!empty($_POST['surname']) && !empty($_POST['firstname']) && !empty($_POST['email']) && !empty($_POST['message'])){
        $surname = htmlspecialchars($_POST['surname']);
        $firstname = htmlspecialchars($_POST['firstname']);
        $email = htmlspecialchars($_POST['email']);
        $message = htmlspecialchars($_POST['message']);

        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
        $body = [
            'Messages' => [
                [
                  'From' => [
                    'Email' => "julien.segard12@gmail.com",
                    'Name' => "Julien"
                  ],
                  'To' => [
                    [
                      'Email' => "julien.segard12@gmail.com",
                      'Name' => "Julien"
                    ]
                  ],
                'Subject' => "Demande de renseignement",
                'TextPart' => '$email, $message', 
                // 'HTMLPart' => "TEXT EMAIL",
                // 'CustomID' => "AppGettingStartedTest"
            ]
            ]
        ];
            $response = $mj->post(Resources::$Email, ['body' => $body]);
            $response->success();
            echo "Email envoyé avec succès !";
        }
        else{
            echo "Email non valide";
        }

    } else {
        header('Location: index.php');
        die();
    }
